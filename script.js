

class App {

    constructor() {
        /** @type {HTMLTableElement} */
        this.tblCustomers = document.getElementById('tblCustomers')

        /** @type {HTMLButtonElement} */
        this.btnPrevious = document.getElementById('btnNext')

        /** @type {HTMLButtonElement} */
        this.btnNext = document.getElementById('btnNext')

        /** @type {HTMLSpanElement} */
        this.lblPageInfo = document.getElementById('lblPageInfo')

        this.mdlCustomer = document.getElementById('mdlCustomer')

        /** @type {HTMLButtonElement} */
        this.btnAddCustomer = document.getElementById('btnAddCustomer')

        /** @type {HTMLButtonElement} */
        this.btnModalClose = document.getElementById('btnModalClose')

        /** @type {HTMLFormElement} */
        this.frmCustomer = document.getElementById('frmCustomer')



        window.addEventListener('load', this.onLoad.bind(this))
    }

    closeModal() {

        this.mdlCustomer.classList.add('dismiss')
    }

    openModal() {
        this.mdlCustomer.classList.add('active')
    }

    /** @param customers {Array<{ id?: string, name: string, email: string, cpf: string, createdAt: Date }>} */
    loadCustomers() {

        const customers = this.getCustomers()

        if(!customers || !customers.length) {
            this.tblCustomers.tBodies[0].innerHTML = `
                <tr>
                    <td colspan="5" style="text-align: center;">Não há linhas para serem exibidas</td>
                </tr>
            `  
        }
        else {
            
            const customersRows = customers.map(customer => {
                const nameTd = document.createElement('td')
                const emailTd = document.createElement('td')
                const cpfTd = document.createElement('td')
                const createdAtTd = document.createElement('td')
                const actionTd = document.createElement('td')
                const editLink = document.createElement('a')
                const deleteLink = document.createElement('a')

                nameTd.innerHTML = customer.name
                emailTd.innerHTML = customer.email
                cpfTd.innerHTML = customer.cpf
                createdAtTd.innerHTML = moment(customer.createdAt).format('DD/MM/YYYY HH:mm')

                editLink.href = deleteLink.href = '#'

                editLink.innerHTML = 'Editar'
                deleteLink.innerHTML = 'Deletar'

                editLink.className = deleteLink.className = 'action-link'

                editLink.addEventListener('click', () => this.onEdit(customer))
                deleteLink.addEventListener('click', () => this.onDelete(customer))

                actionTd.append(editLink, deleteLink)

                const row = document.createElement('tr')

                row.dataset.id = customer.id

                row.append(nameTd, emailTd, cpfTd, createdAtTd, actionTd)

                return row
            })

            const tbody = this.tblCustomers.tBodies[0]

            tbody.innerHTML = ''

            tbody.append( ...customersRows);
        }
    }

    onEdit(customer) {
        this.frmCustomer.name.value = customer.name
        this.frmCustomer.email.value = customer.email
        this.frmCustomer.key.value = customer.id
        this.frmCustomer.createdAt.value = moment(new Date).format('YYYY-MM-DDTHH:mm')
        this.frmCustomer.cpf.value = customer.cpf

        this.openModal()
    }

    onDelete(customer) {
        const customers = this.getCustomers()
        const index = customers.findIndex(x => x.id == customer.id)
        customers.splice(index, 1)
        
        this.updateCustomers(customers)

        this.loadCustomers()
    }

    onSubmit(ev) {
        ev.preventDefault()

        const customer = {
            id: this.frmCustomer.key.value,
            email: this.frmCustomer.email.value,
            cpf: this.frmCustomer.cpf.value,
            name: this.frmCustomer.name.value,
            createdAt: moment(this.frmCustomer.createdAt.value, 'YYYY-MM-DDTHH:mm').toDate()
        }

        if(this.getErrors(customer)) {
            const customers = this.getCustomers()

            if(customer.id) {
                const index = customers.findIndex(x => x.id == customer.id)
                customers.splice(index, 1, customer)
            }
            else {
                customer.id = uuid.v4()
                customers.push(customer)
            }
    
            this.updateCustomers(customers)
            alert('Atualizado com sucesso')
            this.loadCustomers()
            this.closeModal()
        }
    }

    /**@returns { [] } */
    getCustomers() {
        return JSON.parse(localStorage.getItem('customers-app') || '[]')
    }

    updateCustomers(customers) {
        localStorage.setItem('customers-app', JSON.stringify(customers))
    }

    getErrors(customer) {
        const errors = []
        if(!customer.name) {
            errors.push('Nome é obrigatório')
        }

        if(!customer.email) {
            errors.push('Email é obrigatório')
        }

        if(!customer.cpf) {
            errors.push('Cpf é obrigatório')
        }


        if(customer.cpf && !/\d{3}\.\d{3}\.\d{3}-\d{2}/gi.test(customer.cpf)) {
            errors.push('Cpf Inválido')
        }

        this.removeErrors()

        if(errors.length == 0) return true

        const ulErrors = document.createElement('ul')
        ulErrors.classList.add('errors')

        for(let error of errors) {
            const liError = document.createElement('li')

            liError.innerHTML = error
            liError.classList.add('error')

            ulErrors.append(liError)
        }

        this.frmCustomer.prepend(ulErrors)

        return false
    }

    removeErrors() {
        const lastUlErrors = document.querySelector('.errors')
        if(lastUlErrors) lastUlErrors.remove()
    }

    addCustomer() {
        this.frmCustomer.key.value = ""
        this.frmCustomer.cpf.value = ""
        this.frmCustomer.name.value = ""
        this.frmCustomer.email.value = ""
        this.frmCustomer.createdAt.value = moment(new Date).format('YYYY-MM-DDTHH:mm')
        this.removeErrors()
        this.openModal()
    }

    onCloseModal() {
        this.mdlCustomer.classList.remove('active', 'dismiss')
    }

    onLoad() {
        this.btnModalClose.addEventListener('click', this.closeModal.bind(this))
        this.btnAddCustomer.addEventListener('click', this.addCustomer.bind(this))
        this.frmCustomer.addEventListener('submit', this.onSubmit.bind(this))
        this.mdlCustomer.addEventListener('animationend', ev => ev.animationName === 'fade-out' && this.onCloseModal())
        this.loadCustomers()        
    }   
}

new App()
